Gem::Specification.new do |s|
  s.name = 'type_validator'
  s.version = '0.1.1'
  s.date = '2017-10-04'
  s.summary = 'Type validator validates polymorphic attributes\' type.'
  s.description = 'Type validator validates polymorphic attributes\' type.'
  s.authors = ['Zyrthofar']
  s.email = 'zyrthofar@gmail.com'
  s.files = [
    'lib/type_validator.rb',
    'lib/active_model/locale/en.yml',
    'lib/active_model/validations/type_validator.rb',
    'lib/active_record/validations/type_validator.rb',
  ]
  s.license = 'MIT'
end
