module ActiveRecord
  module Validations
    class TypeValidator < ActiveModel::Validations::TypeValidator
      def validate(record)
        super
        attributes.each do |attribute|
          next unless record.class._reflect_on_association(attribute)

          associated_records = Array.wrap(record.send(attribute))

          if associated_records.present? &&
             associated_records.all?(&:marked_for_destruction?)
            record.errors.add(attribute, :invalid_type, options)
          end
        end
      end
    end

    module ClassMethods
      def validates_type_of(*attr_names)
        validates_with TypeValidator, _merge_attributes(attr_names)
      end
    end
  end
end
