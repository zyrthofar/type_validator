module ActiveModel
  module Validations
    class TypeValidator < EachValidator
      def validate_each(record, attr_name, _value)
        attribute_value = record.read_attribute("#{attr_name}_type".to_sym)
        if attribute_value != options[:class_name]
          record.errors.add(attr_name, :invalid_type, options)
        end
      end
    end

    module HelperMethods
      def validates_type_of(*attr_names)
        validates_with TypeValidator, _merge_attributes(attr_names)
      end
    end
  end
end
